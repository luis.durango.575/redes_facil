import cv2
# Carga la cascada

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# Para capturar video desde la cámara web.
#cap = cv2.VideoCapture('1')

cap = cv2.VideoCapture('Piloto Profesional gasta otra Broma a profe de Autoescuela_Cámara Oculta con Zihara Esteban 2.mp4')


while True:
    #Para leer una imagen se usa img = cv2.imread('foto') y no se usa el frame cap.read
    #img = cv2.imread('personas.png')
    # Read the frame
    _, img = cap.read()
    # Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Detect the faces
    faces = face_cascade.detectMultiScale(gray, 1.9, 4)
    # Dibuja el rectángulo alrededor de cada cara
    for (x, y, w, h) in faces :
        cv2.rectangle(img, (x, y), (x+w, y+h), (0, 0, 254), 2)
    #Contador de personas
    print("Se detectaron  {0} rostros".format(len(faces)))
    texto = 'Numero de personas ' + str(len(faces))
    cv2.putText(img,texto, (10,50), cv2.FONT_HERSHEY_DUPLEX, 1.9, (0, 233, 254), 5)
    # Pantalla
    cv2.imshow('img', img)
    # Stop
    k = cv2.waitKey(30) & 0xff
    if k==27:
        break
cap.release()